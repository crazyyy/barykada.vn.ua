</div><!-- content-block -->
</div><!-- #wrapper -->
<footer class="wrapper">
<div class="mainfooter clearfix">

	<div class="fwidget fwidget-1 fwidget-wide">
		<h5>Про «Барикаду»</h5>
		<p>Сайт «Барикада» створено Громадською організацією «Вінниця: Антикорупційний комітет» з метою інформування вінничан про перебіг антикоррупційних ініціатив у області.</p>
		<p>Ми створюємо умови для викриття та контролю недобросовісних посадовців.</p>
	</div><!-- fwidget-1 -->

	<div class="fwidget fwidget-1 fwidget-short">
		<h5>Про головне</h5>
		<ul>
			<li><a href="#">Викриття корупції</a></li>
			<li><a href="#">Контроль посадовців</a></li>
			<li><a href="#">Пікетування установ</a></li>
			<li><a href="#">Вінницькі службовці</a></li>
		</ul>
	</div><!-- fwidget-2 -->

	<div class="fwidget fwidget-2 fwidget-wide">
		<h5>Про посадовців</h5>
		<ul>
			<li><a href="#">Призначення посадовців</a></li>
			<li><a href="#">Обрання суддів та правоохоронців</a></li>
			<li><a href="#">Голосування по службовим посадам</a></li>
		</ul>
	</div><!-- fwidget-3 -->

	<div class="fwidget fwidget-4 fwidget-short">
		<h5>Про сайт</h5>
		<ul>
			<li><a href="http://barykada.vn.ua/aboutus.htm">Про сайт</a></li>
			<li><a href="http://barykada.vn.ua/komanda.htm">Команда</a></li>
			<li><a href="http://barykada.vn.ua/callback.htm">Зворотній зв’язок</a></li>
			<li><a href="http://barykada.vn.ua/category/news">Новини та події</a></li>
		</ul>
	</div><!-- fwidget-4 -->

	<p class="copy" role="contentinfo">Сайт створено у 2014 році © Всі права захищено.</p>
</div><!-- mainfooter -->
</footer>

<div class="backhead bkblock0n">
</div><!-- backhead bkblock0n -->
	<?php wp_footer(); ?>
</body>
</html>